# B1-Reseau-19

Bachelor 1- Reseau-2019/2020

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale


C:\Users\fordt>ipconfig

Windows IP Configuration


Ethernet adapter Ethernet:

   Media State . . . . . . . . . . . : Media disconnected
   Connection-specific DNS Suffix  . :

Ethernet adapter VirtualBox Host-Only Network:

   Connection-specific DNS Suffix  . :
   Link-local IPv6 Address . . . . . : fe80::e91b:bd93:549a:d280%24
   IPv4 Address. . . . . . . . . . . : 192.168.56.1
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Default Gateway . . . . . . . . . :

Wireless LAN adapter WiFi:

   Connection-specific DNS Suffix  . : auvence.co
   Link-local IPv6 Address . . . . . : fe80::389d:fa05:8320:4d41%2
   IPv4 Address. . . . . . . . . . . : 10.33.1.28
   Subnet Mask . . . . . . . . . . . : 255.255.252.0
   Default Gateway . . . . . . . . . : 10.33.3.253
   

C:\Users\fordt>netstat -rn
  
  8...04 92 26 09 cd 70 ......Realtek PCIe GbE Family Controller
 24...0a 00 27 00 00 18 ......VirtualBox Host-Only Ethernet Adapter
 19...64 5d 86 51 3c 32 ......Microsoft Wi-Fi Direct Virtual Adapter #3
 25...66 5d 86 51 3c 31 ......Microsoft Wi-Fi Direct Virtual Adapter #4
  5...00 50 56 c0 00 01 ......VMware Virtual Ethernet Adapter for VMnet1
 17...00 50 56 c0 00 08 ......VMware Virtual Ethernet Adapter for VMnet8
  2...64 5d 86 51 3c 31 ......Intel(R) Wireless-AC 9560
 21...64 5d 86 51 3c 35 ......Bluetooth Device (Personal Area Network)
  1...........................Software Loopback Interface 1

IPv4 Route Table

Active Routes:
Network Destination        Netmask          Gateway       Interface  Metric
          0.0.0.0          0.0.0.0      10.33.3.253       10.33.1.28     35
        10.33.0.0    255.255.252.0         On-link        10.33.1.28    291
       10.33.1.28  255.255.255.255         On-link        10.33.1.28    291
      10.33.3.255  255.255.255.255         On-link        10.33.1.28    291
        127.0.0.0        255.0.0.0         On-link         127.0.0.1    331
        127.0.0.1  255.255.255.255         On-link         127.0.0.1    331
[...]

IPv6 Route Table

Active Routes:
 If Metric Network Destination      Gateway
  1    331 ::1/128                  On-link
 24    281 fe80::/64                On-link
  2    291 fe80::/64                On-link
 17    291 fe80::/64                On-link
  5    291 fe80::/64                On-link
 17    291 fe80::34ae:b89b:167:6033/128
                                    On-link
  2    291 fe80::389d:fa05:8320:4d41/128
                                    On-link
[...]
Persistent Routes:
  None
  
  
  Par le GUI:
  
  control panel >Network and Internet>Network and Sharing Center>click on       connexion>Details.    
  ![](https://i.imgur.com/ftYYppW.png)
  
  Le gateway dans le réseau ynov est une passerelle (local/internet). 

## 2.Modifications des informations

### A.Modification d'adresse IP (part 1)

Following a route in the control panel we are able to find Wi-Fi or Local Area Connection in adapter settings to be finnaly able to modify our ip adress in the poperties of Internet Protocol Version 4 (TCP/IPv4).

![](https://i.imgur.com/z7h6AiQ.png)

Due to the fact we changed our ip adress to 10.33.1.1 internet connexion was cut off as this ip adress is already used by some one.

### B.nmap

### C.Modification d'adresse IP (part 2)

Due to not being at school while writing this i'm not able to use the Ynov network.

# II.Exploration locale en duo

Apres avoir désactiver les firewalls et en reliant les 2 PC par cable ethernet, nous avons modifiers les ip pour qu'ils soient dans le meme réseau local, le PC A est en 192.168.0.1 et PC B est en 192.168.0.2 avec un masque en /30 ( 255.255.255.252) et pour le pc A
En faisait un ipconfig puis un ping nous pouvons bien voir que les deux PC sont reliés.

Résultat du ipconfig PC1 : Carte réseau Ethernet :

   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.2
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . : 192.168.0.2
   
Résultat du ipconfig PC2 : Carte réseau Ethernet :

   Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   
Résultat du ping : Envoi d’une requête 'Ping'  192.168.0.2 avec 32 octets de données:
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.0.2 : octets=32 temps=1 ms TTL=128

## III. Manipulations d'autres outils/protocoles côté client

### 1.DHCP

*  Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
*   Bail obtenu. . . . . . . . . . . . . . : jeudi 16 janvier 2020 16:22:17
   Bail expirant. . . . . . . . . . . . . : jeudi 16 janvier 2020 17:52:26
* ipconfig/release et ensuite un ipconfig /renew

### 2.DNS

* Avec un ipconfig/all : Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.20
                                       10.33.10.2
                                       8.8.8.8
                                       8.8.4.4

* nslookup google.com : Réponse ne faisant pas autorité :
Name :    google.com
Addresses: 216.58.201.238

* nslookup ynov.com : Réponse ne faisant pas autorité :
Name :    ynov.com
Address:  217.70.184.38




